﻿
namespace Multithread_program
{
    public interface ISummator
    {
        int CalculateSumm(int[] array);
    }
}
