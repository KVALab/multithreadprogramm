﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace Multithread_program
{
    public class SumThread : ISummator
    {
        private int _countThread;
        private int[] _array;

        public SumThread(int countThread)
        {
            _countThread = countThread;
        }

        public int CalculateSumm(int[] array)
        {
            _array = array;
            List<Thread> lt = new List<Thread>();
            List<OneThreadCounter> lc = new List<OneThreadCounter>();

            int block = _array.Length / _countThread;
            int ost = _array.Length % _countThread;

            int last_block = (ost > 0) ? 1 : 0;
            for (int i = 0; i < _countThread - last_block; i++)
                lc.Add(new OneThreadCounter(_array, block * i, block * i + block));
            if (last_block != 0)
                lc.Add(new OneThreadCounter(_array, block * (_countThread - 1), block * (_countThread - 1) + block + ost));

            for (int i = 0; i < _countThread; i++)
            {
                Thread t = new(lc.ElementAt(i).Count);
                t.Start();
                lt.Add(t);
            }
            lt.ForEach(t => t.Join());

            return lc.Sum(s => s.sum);
        }
    }
}
