﻿using System.Linq;

namespace Multithread_program
{
    public class SumLINQ : ISummator
    {
        public int CalculateSumm(int[] array)
        {
            return array.AsParallel().Sum();
        }
    }
}
