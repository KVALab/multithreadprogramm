﻿using System;

namespace Multithread_program
{
    public class OneThreadCounter
    {
        private int _start;
        private int _end;
        private int[] _array;
        public int sum;

        public OneThreadCounter(int[] array, int start, int end)
        {
            _array = new int[end - start + 1];
            Array.Copy(array, start, _array, 0, end - start);
            _start = start;
            _end = end;
        }

        public void Count()
        {
            for (int i = 0; i < _end - _start; i++)
                sum += _array[i];
        }
    }
}
