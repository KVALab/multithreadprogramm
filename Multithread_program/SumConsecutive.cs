﻿
namespace Multithread_program
{
    public class SumConsecutive : ISummator
    {
        public int CalculateSumm(int[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
                sum += array[i];

            return sum;
        }
    }
}
