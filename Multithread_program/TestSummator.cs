﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Multithread_program
{
    partial class Program
    {
        public class TestSummator
        {
            private List<int[]> ListArrays { get; set; }
            public string Results { get; set; }
            protected Stopwatch _stopwatch;

            public TestSummator(List<int> list)
            {
                if (list == null || list.Count == 0)
                    throw new ArgumentNullException("Список пуст");

                _stopwatch = new();

                ListArrays = new List<int[]>();
                list.ForEach(e =>
                {
                    var arr = new int[e];
                    var r = new Random();
                    for (int i = 0; i < arr.Length;  i++)
                        arr[i] = r.Next(1, 10);                    
                    ListArrays.Add(arr);
                });
            }

            public void Test(ISummator summator)
            {
                foreach (var array in ListArrays)
                {
                    _stopwatch.Start();
                    int summ = summator.CalculateSumm(array);
                    _stopwatch.Stop();

                    Results += $"{summator.GetType().Name}, sum => {array.Sum()} = {summ}," +
                        $" {_stopwatch.ElapsedMilliseconds} мс, {_stopwatch.ElapsedTicks} ticks{Environment.NewLine}";
                }
            }

        }
    }
}
